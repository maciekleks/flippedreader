package main

import (
	"gitlab.com/maciekleks/flippedreader"
	"io"
	"os"
	"strings"
)

func main() {
	flipped := flippedreader.NewFlippedReader(strings.NewReader("Ôblykej i kōniec godki, dyć żodyn cie tam niy zno!"))
	chunk := make([]byte, 4)
	for {
		n, err := flipped.Read(chunk)
		if err != nil && err == io.EOF {
			break
		}
		os.Stdout.Write(chunk[:n])
	}
}
