package main

import (
	"gitlab.com/maciekleks/flippedreader"
	"io"
	"os"
	"strings"
)

func main() {
	flipped := flippedreader.NewFlippedReader(strings.NewReader("Ôblykej i kōniec godki, dyć żodyn cie tam niy zno!!!"))
	//flipped := flippedreader.NewFlippedReader(strings.NewReader("ą,Maciek"))
	io.Copy(os.Stdout, flipped)
}
