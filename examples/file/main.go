package main

import (
	"fmt"
	"gitlab.com/maciekleks/flippedreader"
	"io"
	"os"
)

func main() {
	/* For a file:
	cat <<EOF > /tmp/input.txt
	Some text with runes.
	blah blah blah
	hello, 世界
	pyrsk!
	EOF

	program ends with:

	!ksryp
	界世 ,olleh
	halb halb halb
	.senur htiw txet emoS
	*/
	f, err := os.Open("/tmp/input.txt")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer f.Close()

	flipped := flippedreader.NewFlippedReader(f)
	io.Copy(os.Stdout, flipped)
}
