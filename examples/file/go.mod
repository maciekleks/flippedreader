module go-programming/flippedreader/examples/file

go 1.20

require gitlab.com/maciekleks/flippedreader v0.2.3

replace gitlab.com/maciekleks/flippedreader v0.0.0 => ../../
