package flippedreader

import (
	"io"
	"unicode/utf8"
)

// NewFlippedReader returns new FlippedReader reading from rs.
func NewFlippedReader(rs io.ReadSeeker) io.Reader {
	return &FlippedReader{rs: rs, cur: -1}
}

// FlippedReader stores io.ReadSeeker interface implementation and current position indicator.
type FlippedReader struct {
	// rs is io.ReadSeaker type holder for the input stream.
	rs io.ReadSeeker
	// cur holds the current position in the source stream.
	cur int64
}

// Read function satisfies io.Reader interface. Reader reads up to n bytes (0 <= n <= len(p)) into p but in case of regular ASCII encoding (no UTF-8)
// n is no greater than len(p) - 3. 3 bytes are reserved in p to encode rune, e.g. ż needs 2 bytes, while 世 needs for 4 bytes to be encoded.
func (fr *FlippedReader) Read(p []byte) (n int, err error) {
	if fr.cur < 0 {
		cur, err := fr.rs.Seek(0, io.SeekEnd)
		if err != nil && err != io.EOF {
			return 0, err
		}
		fr.cur = cur
	}

	var offset = int64(len(p))
	if fr.cur-offset < 0 {
		offset = fr.cur
	}

	_, err = fr.rs.Seek(-offset, io.SeekCurrent)

	if err != nil {
		return 0, err
	}

	src := make([]byte, offset)
	var loaded int
	loaded, err = fr.rs.Read(src)     //moves cursor position in fr.rs by loaded bytes
	tmp := make([]byte, utf8.MaxRune) //4
	k := utf8.MaxRune - 1             //3

	for i := loaded - 1; i >= 0; i-- {
		var b byte = src[i]
		if b < utf8.RuneSelf {
			p[n] = b
			n++
		} else if utf8.RuneStart(b) {
			tmp[k] = b
			r, _ := utf8.DecodeRune(tmp[k:])
			if r == utf8.RuneError {
				return n, err
			}
			n += utf8.EncodeRune(p[n:], r)
			k = utf8.MaxRune - 1
		} else {
			tmp[k] = b
			k--
		}
	}

	if fr.cur == 0 && n == 0 {
		err = io.EOF
	} else {
		fr.cur = fr.cur - int64(n)            //only encoded bytes counts
		fr.rs.Seek(int64(-n), io.SeekCurrent) //move to the beginning of the last read rune
	}

	return n, err
}
