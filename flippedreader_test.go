package flippedreader

import (
	"io"
	"strings"
	"testing"
)

func TestFlippedReader_Read(t *testing.T) {
	type fields struct {
		rs  io.ReadSeeker
		cur int64
	}
	type args struct {
		p []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantN   int
		wantS   string
		wantErr bool
	}{
		{
			name:    "word",
			fields:  fields{rs: strings.NewReader("dyć"), cur: -1},
			args:    args{p: make([]byte, 10)}, //reserve more for rune, at least 4 bytes than the string len
			wantN:   4,                         //4 bytes d->1 byte, y->1 byte, ć->2 bytes
			wantS:   "ćyd",
			wantErr: false,
		},
		{
			name:    "sentence",
			fields:  fields{rs: strings.NewReader("Ôblykej i kōniec godki, dyć żodyn cie tam niy zno!"), cur: -1},
			args:    args{p: make([]byte, 100)}, //4 bytes reserved for rune
			wantN:   54,
			wantS:   "!onz yin mat eic nydoż ćyd ,ikdog ceinōk i jekylbÔ",
			wantErr: false,
		},
		{
			name:    "char-error",
			fields:  fields{rs: strings.NewReader("ō"), cur: -1},
			args:    args{p: make([]byte, 1)}, //you should reserve at least 2 bytes for rune
			wantN:   0,
			wantErr: false,
		},
		{
			name:    "char-ok",
			fields:  fields{rs: strings.NewReader("ō"), cur: -1},
			args:    args{p: make([]byte, 5)},
			wantN:   2, //ō takes 2 bytes
			wantS:   "ō",
			wantErr: false,
		},
		{
			name:    "resizing",
			fields:  fields{rs: strings.NewReader("界"), cur: -1},
			args:    args{p: make([]byte, 3)}, //you should reserve at least 3 bytes
			wantN:   3,                        //界 takes 3 bytes
			wantS:   "界",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fr := &FlippedReader{
				rs:  tt.fields.rs,
				cur: tt.fields.cur,
			}
			gotN, err := fr.Read(tt.args.p)
			if (err != io.EOF && err != nil) != tt.wantErr {
				t.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotN != tt.wantN {
				t.Errorf("Read() gotN = %v, want %v", gotN, tt.wantN)
			}
			if string(tt.args.p[:tt.wantN]) != tt.wantS {
				t.Errorf("Read() %v, want %v", tt.args.p, []byte(tt.wantS))
			}
		})
	}
}

func BenchmarkFlippedReader_Read(b *testing.B) {
	for i := 0; i < b.N; i++ {
		flipped := NewFlippedReader(strings.NewReader(`
1. Zostow, dyć to je dlo gości!
2. Ôblykej i kōniec godki, dyć żodyn cie tam niy zno.
3. Niy śpiywej przi jedzyniu, bo bydziesz mieć gupigo chopa.
4. Pojodłeś? Tōż fōns utrzij a nazod do heftōw.
5. Przeżegnej sie krziżym świyntym, a proś Pōnbōczka ô lepszy rozum.
6. Lygej, bo gasza!
7. Stroć mi sie stōnd!
8. Kartofle ôstow, bele miynso zjysz.
9. Gyszynki ôd dzieciōntka? Nojprzōd barbōrka i szternostka.
10. Jak niy świnia, to sie wrōci.
`))
		_, err := io.ReadAll(flipped)
		if err != nil {
			b.Errorf("Benchmark error: %v", err)
		}
	}
}
