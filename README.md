# What is flippedreader

Backward reader (satisfies [io.Reader](https://pkg.go.dev/io#Reader) interface) with rune support, e.g. giving
"ąęść" on the input you get "ćśęą".

# Examples

```go
package main

import (
	"gitlab.com/maciekleks/flippedreader"
	"io"
	"os"
	"strings"
)

func main() {
	flipped := flippedreader.NewFlippedReader(strings.NewReader("Ôblykej i kōniec godki, dyć żodyn cie tam niy zno!"))
	io.Copy(os.Stdout, flipped)
	// Output: !onz yin mat eic nydoż ćyd ,ikdog ceinōk i jekylbÔ
}
```

More in `examples`.

# Constraints

- does not support CR/LF